package com.example.demo.controller;

import com.example.demo.model.EncryptedMessage;
import com.example.demo.service.MessageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author jakub
 * 31.05.2023
 */
@RestController
@RequestMapping("api/v1/message/")
public class MessageController {

    private MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/encrypt")
    public ResponseEntity<EncryptedMessage> encrypt(@RequestBody Map<String, Object> payload) {
        return ResponseEntity.ok().body(messageService.encrypt(payload));
    }

    @PostMapping("/decrypt")
    public ResponseEntity<Map<String, Object>> decrypt(@RequestBody EncryptedMessage message) {
        return ResponseEntity.ok().body(messageService.decrypt(message));
    }

}
