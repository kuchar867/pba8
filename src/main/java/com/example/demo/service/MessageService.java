package com.example.demo.service;

import com.example.demo.model.EncryptedMessage;
import com.example.demo.utils.EncryptUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author jakub
 * 31.05.2023
 */
@Service
public class MessageService {

    private ObjectMapper objectMapper;
    private KeyProviderService keyProviderService;

    public MessageService(ObjectMapper objectMapper, KeyProviderService keyProviderService) {
        this.objectMapper = objectMapper;
        this.keyProviderService = keyProviderService;
    }

    public EncryptedMessage encrypt(Map<String, Object> payload) {
        String stringPayload = extractPayload(payload);

        try {
            Map<String, String> encryptedPayload = EncryptUtils.encrypt(stringPayload, keyProviderService.getPublicKey());
            return new EncryptedMessage(encryptedPayload.get("message"), encryptedPayload.get("key"));
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public Map<String, Object> decrypt(EncryptedMessage encryptedMessage) {
        try {
            String decryptedPayload = EncryptUtils.decrypt(encryptedMessage, keyProviderService.getPrivateKey());
            return restoreMessage(decryptedPayload);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private String extractPayload(Map<String, Object> payload) {
        String stringPayload;
        try {
            stringPayload = objectMapper.writeValueAsString(payload);
        } catch (JsonProcessingException jsonProcessingException) {
            return null;
        }
        return stringPayload;
    }

    private Map<String, Object> restoreMessage(String messageString) {
        try {
            return objectMapper.readValue(messageString, new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            throw new IllegalStateException();
        }
    }
}
