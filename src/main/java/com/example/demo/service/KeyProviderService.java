package com.example.demo.service;

import org.springframework.stereotype.Service;

import java.security.*;

/**
 * @author jakub
 * 31.05.2023
 */
@Service
public class KeyProviderService {

    private final KeyPair keyPair;

    public KeyProviderService() throws NoSuchAlgorithmException {
        keyPair = getKeyPair();
    }

    public PrivateKey getPrivateKey() {
        return keyPair.getPrivate();
    }

    public PublicKey getPublicKey() {
        return keyPair.getPublic();
    }

    private KeyPair getKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);

        return keyPairGenerator.generateKeyPair();
    }
}
