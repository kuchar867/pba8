package com.example.demo.exception;

import com.example.demo.model.Error;
import com.example.demo.utils.ResponseHeaderProvider;
import org.springframework.http.HttpStatus;

/**
 * @author jakub
 * 18.04.2023
 */

public class ErrorFactory {
    public static Error createError(HttpStatus httpStatus) {
        Error error = new Error();
        error.setCode(String.valueOf(httpStatus.value()));
        error.setMessage(httpStatus.getReasonPhrase());
        error.setResponseHeader(ResponseHeaderProvider.provide());
        return error;
    }
}
