package com.example.demo.entity;

import com.example.demo.model.User;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author jakub
 * 12.04.2023
 */
@Entity
@Table(name = "USER_TABLE")
public class UserEntity implements BaseEntity<String> {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id = null;

    @NotNull
    private String name = null;

    @NotNull
    private String surname = null;

    @Min(1)
    private Integer age = null;

    @NotNull
    @Pattern(regexp = "^[0-9]{11}$")
    private String personalId = null;

    @NotNull
    private User.CitizenshipEnum citizenship;

    @Pattern(regexp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")
    private String email = null;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public User.CitizenshipEnum getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(User.CitizenshipEnum citzenship) {
        this.citizenship = citzenship;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
