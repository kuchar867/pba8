package com.example.demo.utils;

import com.example.demo.entity.UserEntity;
import com.example.demo.model.*;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * @author jakub
 * 15.04.2023
 */
@Service
public class UserMapper {
    private final ObjectMapper<UserEntity, User> objectMapper;

    public UserMapper() {
        this.objectMapper = new ObjectMapper<>();
    }

    public UserListResponse mapToListResponse(List<UserEntity> entities) {
        UserListResponse userListResponse = new UserListResponse();
        userListResponse.setUsersList(objectMapper.mapToDtoList(entities, User.class));
        userListResponse.setResponseHeader(createResponseHeader());
        return userListResponse;
    }

    public UserResponse mapToResponse(UserEntity entity) {
        UserResponse userResponse = new UserResponse();
        userResponse.setUser(objectMapper.mapToDto(entity, User.class));
        userResponse.setResponseHeader(createResponseHeader());
        return userResponse;
    }

    public UserEntity mapToEntity(CreateRequest request) {
        return objectMapper.mapToEntity(request.getUser(), UserEntity.class);
    }

    public UserEntity mapToEntity(UpdateRequest request) {
        return objectMapper.mapToEntity(request.getUser(), UserEntity.class);
    }


    private RequestHeader createResponseHeader() {
        RequestHeader responseHeader = new RequestHeader();
        responseHeader.setRequestId(UUID.randomUUID());
        responseHeader.setSendDate(DateTime.now());
        return responseHeader;
    }
}
