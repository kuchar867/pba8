package com.example.demo.utils;

import com.example.demo.model.EncryptedMessage;
import org.apache.commons.lang3.RandomStringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Map;

/**
 * @author jakub
 * 31.05.2023
 */
public class EncryptUtils {
    private static final String RSA = "RSA";
    private static final String AES = "AES";

    private static final String ALGORITHM = "PBKDF2WithHmacSHA256";
    private static final int AES_KEY_SIZE = 256;


    public static String decrypt(EncryptedMessage message, PrivateKey privateKey) throws Exception {
        String decryptedSecret = EncryptUtils.decryptRSA(message.getKey(), privateKey);
        String decryptedAES = EncryptUtils.decryptAES(message.getMessage(), decryptedSecret);
        return EncryptUtils.decryptRSA(decryptedAES, privateKey);
    }

    public static Map<String, String> encrypt(String payload, PublicKey publicKey) throws Exception {
        String encryptedRSA = EncryptUtils.encryptRSA(payload, publicKey);
        SecretKey secretKey = EncryptUtils.generateRandomSecret();

        return Map.of(
                "message", EncryptUtils.encryptAES(encryptedRSA, secretKey),
                "key", EncryptUtils.encryptRSA(Base64.getEncoder().encodeToString(secretKey.getEncoded()), publicKey)
        );
    }

    private static String encryptRSA(String plaintext, Key publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance(RSA);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return Base64.getEncoder().encodeToString(cipher.doFinal(plaintext.getBytes(StandardCharsets.UTF_8)));
    }

    private static String decryptRSA(String ciphertext, Key privateKey) throws Exception {
        byte[] encryptedBytes = Base64.getDecoder().decode(ciphertext);
        Cipher cipher = Cipher.getInstance(RSA);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(encryptedBytes), StandardCharsets.UTF_8);
    }

    private static String encryptAES(String plaintext, SecretKey secret) throws Exception {
        Cipher cipher = Cipher.getInstance(AES);
        cipher.init(Cipher.ENCRYPT_MODE, secret);
        return Base64.getEncoder().encodeToString(cipher.doFinal(plaintext.getBytes(StandardCharsets.UTF_8)));
    }

    private static String decryptAES(String ciphertext, String secret) throws Exception {
        byte[] decodedKey = Base64.getDecoder().decode(secret);
        SecretKey secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, AES);
        Cipher cipher = Cipher.getInstance(AES);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] encryptedBytes = Base64.getDecoder().decode(ciphertext);
        return new String(cipher.doFinal(encryptedBytes), StandardCharsets.UTF_8);
    }

    private static SecretKey generateRandomSecret() throws NoSuchAlgorithmException, InvalidKeySpecException {
        String randomSecret = RandomStringUtils.random(20, true, true);
        SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGORITHM);
        KeySpec spec = new PBEKeySpec(randomSecret.toCharArray(), randomSecret.getBytes(), 65536, AES_KEY_SIZE);
        return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), AES);
    }
}
