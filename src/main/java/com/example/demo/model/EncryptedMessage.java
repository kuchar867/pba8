package com.example.demo.model;

/**
 * @author jakub
 * 31.05.2023
 */
public class EncryptedMessage {
    private String message;
    private String key;

    public EncryptedMessage(String message, String key) {
        this.message = message;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public String getMessage() {
        return message;
    }
}
