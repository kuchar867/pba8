package com.example.demo.authentication;

import com.example.demo.authentication.model.Token;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author jakub
 * 25.04.2023
 */
public interface AuthenticationClient {
    @PostMapping(value = "/oauth/token?grant_type=client_credentials")
    ResponseEntity<Token> authenticationRequest();
}
